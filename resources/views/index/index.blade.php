@extends('layouts.app')

@section('content')
<div class="container">


    <div class="row justify-content-center">
    	<div class="col-md-4">
    		<div class="jumbotron">
  				<h1>DSS</h1>
  				<p>Domain Sale System <span>Version: <a href="https://gitlab.com/sequential1/dss" target="_blank">0.1 Alpha</a></span></p>
  				<p>Thank You for using this software. We created this system to help sell domain or more than one and is completely free.</p>
                <p>DSS is designed for minimum requirements from user, let me show you this in three simple steps</p>
			</div>
    	</div>
    	
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Domain Market</div>
                <div class="card-body">
                   <p>After you add any domain this site will be replaced with domain market list. Any single domain will be redirected immediately to offer page. There user 
                   can send a price offer using contact form. Click <a href="">here</a> to see example of offer page.</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
