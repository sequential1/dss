
<?php
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
   
    # config
    $error_status = 0;
    $host = 'localhost';
    $port = '3306';
    $username = 'root';
    $password = '';
    $db_name = 'dss' . rand(0x029A, 0x270F);
    $options    = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
    ];
    
    function updateEnv($data = []) {
        
        if (!count($data)) {
            return;
        }
        
        $pattern = '/([^\=]*)\=[^\n]*/';
        
        $envFile = str_replace('public', '', $_SERVER['DOCUMENT_ROOT']) . '.env';
        $lines = file($envFile);
        $newLines = [];
        
        foreach ($lines as $line) {
            preg_match($pattern, $line, $matches);
            
            if (!count($matches)) {
                $newLines[] = $line;
                continue;
            }
            
            if (!key_exists(trim($matches[1]), $data)) {
                $newLines[] = $line;
                continue;
            }
            
            $line = trim($matches[1]) . "={$data[trim($matches[1])]}\n";
            $newLines[] = $line;
        }
        
        $newContent = implode('', $newLines);
        file_put_contents($envFile, $newContent);
    }
    
?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>DSS - Domain Sale System</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="../css/app.css" rel="stylesheet">
</head>
<body>
	<style>
        td { padding-bottom: 15px; }
        label { font-weight: bold; }
        .pull-right { float: right; }
        .pull-left { float: left; }
        .clear { clear: both; }
	</style>
    
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="/install">Domain Sale System</a>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <div class="card-header">
                            	Install Wizard | <a href="/">Home</a>
                            	<img class="pull-right" width="24px" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAQAAAAAYLlVAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfjCwUOEyhCzsbNAAAEpElEQVRo3t2Zb0xVZRjAf7erlKiIV0DTMgMMhQGj1XAFbDqMrSXrj7W5zFputdrK9Wc615fmF1errZEusz60pFqBRmY62iiazrJos6QQmC2CmcAFFmlAcTl98HTue855n3suXs69W8/5ct/zPu/z/M7773ne90KKJRi35h1spAjox0gN6ncYGBj08QSzUwFQzLCJYHCSnFQgVHDJQviJualAqLUADN5OBcAcJhWEkpkwedU0dNN4x7Zqtvr7tQvYzXVKOZ2jytcbGLT7C1CLQQfZZimTEw73Bhf9BbgfA4M2MoDFnHa5N/h7WgM4baky3ZxiG+c07g16/O2BDNuM1z0jFPiL0OQBYNDLDX4CVBDxRIhOU19ktyeAwWky/QOYxb44EFqZ42cvPMYFT4TDzLpS894JyffspYdRgoQZFEa8gBtp8rMX/pMAb4m9sDcZABDkIxHhxeQgpHFMRHg2OQjpmsB0+Zni0eQgZGqDk4HBJPclB2EpvwgIE6xPDkIe5wWEP7glfjOBmLXz2cRCq3SJBvqV2mJaCWnbhamiI/FvDPCtKwOYZ9Mo508xTq5IHCBXY7jaoVPNuIDQzeJEAYo0ZmtdWveIicsPyvD5CACPMCUgnEzk/FRCg8bkF1RqdHeKe+On8R9k8/iYFjYRAJbQJH6VwXEzF1zPUZq5FYCXRO36+HLn2bSbDU7wJAMe8f8iz/OJ+XuQEBBgf2Jx8hnPpEN+6gAI8uGVx8ks5ezvfH7jGPUcoUvU+IciIKE4+Ya2UYR6yhStm6hjQqv5uakxj28EgCl2spmt3E66232xdiUPUAXA1ayhlgpzDyyiW+tgg2krxBmPARtnP7l2gFc0aoPkA4uoY9R8M8YBlgNZdGr0D1rWcrT19meMzSrADk2HrQNW86vj/RBVwCrGXC3etC3o854IUzwebTCXRkd1I7BQG/FHyANedrw97ri0KokxqaNDoc4vavhZqazUOFE7e5myTfXxoCaoy3HSPXVNediqCBMkyKDQbJJsoreGBruElVUjrBh1GArUO6JrrV/dRCggSzAcpBzotMpLBL1mDgg1Q4wDEKBKBYiGzmFgEbJkA2GrFBL19JeZ77GMUi4AUKgCjNhMDsUACIPSP7KmLhvYwxYm6KKGESCgAvxu/conSKfyjXaJ8DUodyP9SNLrerOLp5gC4EfuYpRz0ap1th2sAnkVNGJfBT08IKS2Lzim3DZHvXWoT+cDh5MGpH1gmFzc0f9L7al5he2GZbs8pts1C2QtUDiNnXCf1rK6wW2QAV7VfOkAeUAWr1sbyjj1LAdCnNXoH9JazlY25T45US/VRsN+M/u7htu4l0rmA1Ao5AV3C7bXKBvaWflfBn06FeFdShWtfF4TdrcWuXvJo9XSa3Nf5Vyevzl0sUAw0EM7YUKsZJWgEaGMM8SSajZSSQ4j3CzdMD/nGTrkZw8zIGl0mOZO8TRhD5d/sYPPrNCVlbh7gJUcoZUtBIClHI7h/iSrAbiTZloonxn3binjkMb5V6z1y6Fb4j0bJiS+/uWQKMBYnO98hGtzDECvuR/OoMS+I8rgIa63SgO8b2Yy/yf5F234vLtetXJ/AAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDE5LTExLTA1VDE0OjE5OjQwKzAwOjAw+KkKFwAAACV0RVh0ZGF0ZTptb2RpZnkAMjAxOS0xMS0wNVQxNDoxOTo0MCswMDowMIn0sqsAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC">
                            	<div class="clear"></div>
                            </div>
                            <div class="card-body">
                            	<p>Welcome To Domain Sale System. We need information on the database. Please provide all requeried data and if you can, change rights to write <b>.env</b> file in main directory. Don't worry if you can't you will update database access manually later.</p>
                              	
                              	<?php if(empty($_POST)): ?>
                                    <form method="post">
                                    	<table>
                                    		<tr>
                                    			<td width="150px"><label for="host">Host</label></td>
                                    			<td width="180px" style="margin-bottom: 100px;"><input type="text" name="host" id="host" placeholder="localhost"></td>
                                    			<td>Database server address or IP. If is on the same server you can leave localhost.</td>
                                    		</tr>
                                    		<tr>
                                    			<td><label for="port">Port</label></td>
                                    			<td><input type="text" name="port" id="port" placeholder="3306"></td>
                                    			<td>Database Port, standard port for MySQL is 3306.</td>
                                    		</tr>
                                    		<tr>
                                    			<td><label for="username">Username</label></td>
                                    			<td><input type="text" name="username" id="username" placeholder="root"></td>
                                    			<td>Your Database User</td>
                                    		</tr>
                                    		<tr>
                                    			<td><label for="password">Password</label></td>
                                    			<td><input type="password" name="password" id="password"></td>
                                    			<td>Your Database Password</td>
                                    		</tr>
                                    		<tr>
                                    			<td><label for="db-name">Database Name</label></td>
                                    			<td><input type="text" name="db_name" id="db-name"></td>
                                    			<td>Database name, optional if you leave blank system generate name like (dss[random numbers])</td>
                                    		</tr>
                                    		<tr>
                                    			<td></td>
                                    			<td><input type="submit" name="submit" value="Install"></td>
                                    			<td>If you like this software you can <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=EF2JLAJ4FFU9J&source=url" target="_blank"><b>support</b></a> author.</td>
                                    		</tr>
                                    	</table>
                                	</form>
                                	
                                <?php else:
                                    
                                    # set values or deafult
                                    $host = (empty($_POST['host'])) ? $host : $_POST['host']; 
                                    $port = (empty($_POST['port'])) ? $port : $_POST['port'];
                                    $username = (empty($_POST['username'])) ? $username : $_POST['username'];
                                    $password = (empty($_POST['password'])) ? $password : $_POST['password'];
                                    $db_name = (empty($_POST['db_name'])) ? $db_name : $_POST['db_name'];
                                   
                                    # try connect to db
                                    try {
                                        $connection = new PDO("mysql:host=$host", $username, $password, $options);
                                    } catch(PDOException $ex) {
                                        $error_status = 1;
                                        echo '<hr><b>Error: Can not connet to Database</b><br>';
                                        echo '<hr><button type="button" onclick="history.back();">Back</button>';
                                    }
                                    
                                    if (!$error_status) {
                                        # get sql file
                                        $sql = file_get_contents("init.sql");
                                        
                                        try { 
                                            $connection->exec("CREATE DATABASE {$db_name}");
                                            $connection->exec("USE {$db_name}");
                                            $connection->exec($sql);   
                                            $connection->exec("INSERT INTO users(name, email, password, active) VALUES('admin', 'admin@test.test', '" . password_hash('admin', PASSWORD_BCRYPT, ['cost' => 12]) . "', 1)");            
                                        } catch(PDOException $ex) {
                                            $error_status = 1;
                                            echo '<hr><b>Error: This Database already exists, Please delete old database, change name or leave blank.</b><br>';
                                            echo '<hr><button type="button" onclick="history.back();">Back</button>';
                                        }
                                    }
                                    
                                    if (!$error_status) {
                                        # check .env file and update db if cant show info with config
                                        if (file_exists($path = str_replace('public', '', $_SERVER['DOCUMENT_ROOT']) . '.env') && is_writable($path)) {
                                            
                                            updateEnv([
                                                'DB_HOST' => $host,
                                                'DB_PORT' => $port,
                                                'DB_DATABASE' => $db_name,
                                                'DB_USERNAME' => $username,
                                                'DB_PASSWORD' => $password,
                                            ]);
                                      
                                        } else {
                                            
                                            echo "<pre>";
                                            echo "DB_CONNECTION=mysql\n";
                                            echo "DB_HOST={$host}\n";
                                            echo "DB_PORT={$port}\n";
                                            echo "DB_DATABASE={$db_name}\n";
                                            echo "DB_USERNAME={$username}\n";
                                            echo "DB_PASSWORD={$username}\n";
                                            echo "</pre>";
                                        }
                                        
                                        echo "<hr><p>Thank You for install our <b>DS - System</b>, this is access to your account:<p>";
                                        echo "<ul><li>Login: <b>admin@test.test</b></li><li>Password: <b>admin</b></li></ul>";
                                        echo "<hr><p>After first login, please change your password and login. Please remove <i><b>Install</b></i> folder from /public/install</p>";
                                        echo "<p><a href='/'>Go to Home page</a></p>";
                                    }
                                    
                                endif; ?>
                                		
                            </div>
                            <div class="card-footer">
                            	<div class="pull-left">
                            		<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/" target="_blank">
                            			<img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFAAAAAPCAIAAAD8q9/YAAAABGdBTUEAANbY1E9YMgAAAZJJREFUSMflljFPwkAUgN8P8A47ssGpwYlIbiLEgXIbrp1IHMvgpIM3kRAHYMMElvsL56QuBgYnCCEX/wF/4f7CGT1SsZYKCZjUvnS4NPfafvneez2AFIZJTXwBPzzK1at51QSAXq8XmcYYwxgPxcBunr/NM4eZRHiNBh6KAcZ4HW3AXDgt2P2j11G73U4wcOW8ks/n42tDKQUAjcuGTZmpWf2inlRghJHVq5QihAAAY0xrzRgDAEKIUspKLtFSUNX9+37kXAi/7Ps6PmXdzl0Ct+5aADAej40xhBDP84wx/DMcx9FaCyGklPYmwiio6sl0Evn00CKGNj59X8A3t9cAoJTSWq/OLc/zKKWrVc05xxjbrOeXp+l8GqMr0nNow0+9u5W8hWHGmO/71jDnXAixNIy2MLyhrlAh7B34o4fRsoellI7jAIDv+0EPU0oXi4UxplarFc+KG/bwr1+/ruf/ArhcKedyuRRN6aEYIHTQ7XZjgF3XPT45+if/4WB0dTqdSNqqW81ms8k+aaXrLJ2qeAcov3RMWOBuwAAAAABJRU5ErkJggg==">
                            		</a>
                            	</div>
                            	<div class="pull-right">Powered by <a href="https://sequential1.ltd" target="_blank">Sequential 1 Ltd.</a></div>
                            	<div class="clear"></div>
                            	<div class="pull-right"><small>Ver <a href="https://gitlab.com/sequential1/dss" target="_blank">0.1</a> Alpha</small></div>
                            	<div class="clear"></div>
                        	</div>
                        </div>
                    </div>
                </div>
			</div>
        </main>
    </div>
</body>
</html>


