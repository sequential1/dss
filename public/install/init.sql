  CREATE TABLE users (
    id int(11) unsigned NOT NULL AUTO_INCREMENT,
  	name varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  	email varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  	password varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  	remember_token varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  	active tinyint(4) DEFAULT '0',
  	created_at timestamp NULL DEFAULT NULL,
  	updated_at timestamp NULL DEFAULT NULL,
  	deleted_at timestamp NULL DEFAULT NULL,
  	PRIMARY KEY (id),
  	UNIQUE KEY users_email_unique (email)
  );
  
  CREATE TABLE domains (
  	id  int(11) unsigned NOT NULL AUTO_INCREMENT,
  	name varchar(255) DEFAULT NULL,
  	price int(11) unsigned NOT NULL DEFAULT '0',
  	visit int(11) unsigned NOT NULL DEFAULT '0',
  	status bit(1) NOT NULL DEFAULT b'1',
  	created_at timestamp NULL DEFAULT NULL,
  	updated_at timestamp NULL DEFAULT NULL,
  	deleted_at timestamp NULL DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  